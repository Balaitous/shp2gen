#!/bin/env python
# -*- coding: utf-8 -*-

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>


"""
Conversion en format texte d'un fichier shapefile de polygones.
Il est possible d'utiliser un attribut du shapefile comme identifiant des
polygones.

Format de sortie:

    # -b 0
    6.890000 44.360000
    6.810000 44.330000
    ...
    # -b 1
    ...

Les différents polygones constituant un multipolygone sont représentés par
différentes géométries avec le même identifiant.

L'utilisation de ce script nécessite gdal.
Dans la plupart des distributions les paquets requis sont gdal et gdal-python.
Ce script à été testé avec gdal 1.9.2 et semble être incompatible avec gdal 1.7
"""

import argparse
import glob
import os
import sys

from osgeo import ogr, osr

parser = argparse.ArgumentParser(description=__doc__)
parser.add_argument(
    '--shp', dest='shp',
    help="Ficher shapefile à convertir (avec l'extenstion .shp)",
    required=True)
parser.add_argument(
    '--gen', dest='gen',
    help="Fichier .gen de sortie",
    required=False)
parser.add_argument(
    '--id', dest='id',
    help="Nom de l'attribut a utiliser en tant qu'ID des polygones. "
         "Cet attribut doit être de type entier. "
         "Si aucun attribut n'est indiqué, un ID est automatiquement généré.",
    required=False)
parser.add_argument(
    '--title', dest='title',
    help="Attribut du fichier shapefile utilisé comme titre pour générer le "
         "fichier .txt de métadonnées associé au .gen")


class Shapefile(object):
    """
    Gestion des noms de fichier shapefile.
    """

    def __init__(self, filename):
        """
        Constructeur
        @param filename Nom du fichier shapefile avec ou sans extension.
        """
        for item in glob.glob("%s*" % filename):
            rootname, ext = os.path.splitext(item)
            ext = ext[1:].lower()
            if ext == 'shp':
                path, basename = os.path.split(rootname)
                ## Chemin du shapefile
                self.path = path
                ## Nom de base
                self.basename = basename
                ## Nom avec l'extension shp
                self.shp = item
                return
        raise IOError("File not found: %s" % filename)


args = parser.parse_args()

shapefile = Shapefile(args.shp)
if args.gen is None:
    gen_filename = shapefile.basename
else:
    gen_filename = args.gen
    if gen_filename.split('.')[-1].lower() == 'gen':
        gen_filename = '.'.join(gen_filename.split('.')[0:-1])

ds = ogr.Open(shapefile.shp)
if ds is None:
    print("Ouverture de %s impossible" % shapefile.shp)
    sys.exit(1)

layer = ds.GetLayer()
layer.ResetReading()

epsg4326 = osr.SpatialReference()
epsg4326.SetFromUserInput("EPSG:4326")

gen_file = open(gen_filename + ".gen", "w")
if args.title is not None:
    title_file = open(gen_filename + ".txt", "w")

features = [feature for feature in layer]
if args.id is not None:
    features.sort(key=lambda x: x.GetField(args.id))

for index, feature in enumerate(features):

    geom = feature.GetGeometryRef()
    if geom is None:
        continue
    if geom.GetGeometryType() == ogr.wkbPolygon:
        polygons = [geom]
    elif geom.GetGeometryType() == ogr.wkbMultiPolygon:
        polygons = geom
    else:
        raise ValueError("Type de géométrie non traité: %s" %
                         geom.GetGeometryName())
    if args.id is None:
        feat_id = index
    else:
        feat_id = feature.GetField(args.id)
    if args.title is not None:
        title = feature.GetField(args.title)
        title_file.write("%i,'%s'\n" % (feat_id, title.replace("'", "\'")))
    for polygon in polygons:
        polygon.TransformTo(epsg4326)
        for ring in polygon:
            gen_file.write("# -b %i\n" % feat_id)
            for pt in ring.GetPoints():
                gen_file.write("%.6f %.6f\n" % (pt[0], pt[1]))

gen_file.close()
if args.title is not None:
    title_file.close()
