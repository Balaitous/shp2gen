Conversion en format texte d'un fichier shapefile de polygones.
Il est possible d'utiliser un attribut du shapefile comme identifiant des
polygones.

Format de sortie:

    # -b 0
    6.890000 44.360000
    6.810000 44.330000
    ...
    # -b 1
    ...

Les différents polygones constituant un multipolygone sont représentés par
différentes géométries avec le même identifiant.

L'utilisation de ce script nécessite gdal.
Dans la plupart des distributions les paquets requis sont gdal et gdal-python.
Ce script à été testé avec gdal 1.9.2 et semble être incompatible avec gdal 1.7
